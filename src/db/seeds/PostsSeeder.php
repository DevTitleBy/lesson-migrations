<?php


use Phinx\Seed\AbstractSeed;

class PostsSeeder extends AbstractSeed
{
    public function run(): void
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 100; $i++){
            $name = $faker->name();

            $data[] = [
                'title' => $name,
                'description' => $faker->paragraph(rand(5, 15)),
                'alias' => $name,
                'home' => $faker->randomElement(['yes', 'no']),
            ];
        }

        $table = $this->table('posts');
        $table->insert($data)->save();

    }
}
