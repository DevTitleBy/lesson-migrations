<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ChangeColumViewToTablePost extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('posts');
        $table->changeColumn('view', 'integer', array('default' => 1, 'null' => true))->save();
    }

    public function down(): void
    {
        $table = $this->table('posts');
        $table->changeColumn('view', 'integer', array('default' => 0, 'null' => true))->save();
    }
}
