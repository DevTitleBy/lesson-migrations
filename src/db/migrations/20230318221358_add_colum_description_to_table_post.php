<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddColumDescriptionToTablePost extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('posts');

        $table->addColumn('description', 'string', array('limit' => 5000,'after' => 'title'))->save();

    }

    public function down(): void
    {
        $table = $this->table('posts');
        $table->removeColumn('description')->save();
    }
}
