<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreatePostTable extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('posts');

        $table->addColumn('title', 'string', array('limit' => 150))
            ->addColumn('alias', 'string', array('limit' => 50))
            ->addColumn('view', 'integer', array('default' => 0, 'null' => true))
            ->addColumn('home', 'enum', array('values' => array('yes', 'no')))
            ->addColumn('date', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addIndex(array('title', 'alias'), array('unique' => true))
            ->create();
    }

    public function down(): void
    {
        $this->table('posts')->drop()->save();
    }
}
