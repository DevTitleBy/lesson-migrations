Для теста миграций склонировать данный репозиторий, очистить папку src и запустить команду **docker compose up -d**

Примеры написанных миграций находятся в каталоге **src/db/migrations**

Примеры написанных сидов находятся в каталоге **src/db/seeds**

[Инструкция по использованию миграций библиотеки Phinx](https://docs.google.com/document/d/1NgxbhMpzVtnBqa3zTXqnLkNQC4nztwjxsjuInDzA5ew)